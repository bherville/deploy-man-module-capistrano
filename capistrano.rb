class Capistrano < DeployManDeploymentModule
  def pre_deploy(deployment_hash)
    deployment_parameters = merge_hash(deployment_hash)

    if deployment_parameters[:pre_deploy_command]
      exit_statuses = []
      deployment_parameters[:pre_deploy_command].split('||').each do |command|
        update_output_log("Pre Deploy Command: #{command}")

        status = Open4::popen4(command) do |pid, stdin, stdout, stderr|
          update_output_log("Running pre deploy command under PID: #{pid}")

          until stdout.eof? do
            update_output_log(stdout.gets)
            update_output_log(stderr.gets)
          end
        end 

        exit_statuses << status.exitstatus.to_i == 0 ? true : false 
      end
      
      { success: !exit_statuses.include?(false) }
    else
      true      
    end
  end

  def deploy(deployment_hash)
    deployment_parameters = merge_hash(deployment_hash)

    update_output_log("Deployment Parameters: #{deployment_parameters.inspect}")
    update_output_log("Cap Command: #{build_cap_command(deployment_parameters)}")

    status = Open4::popen4(build_cap_command(deployment_parameters)) do |pid, stdin, stdout, stderr|
      update_output_log("Running cap command under PID: #{pid}")

      until stdout.eof? do
        update_output_log(stdout.gets)
        update_output_log(stderr.gets)
      end
    end

    update_output_log("Exit Status: #{status.exitstatus}")

    { success: status.exitstatus.to_i == 0 ? true : false }
  end

  def post_deploy(deployment_hash)
    deployment_parameters = merge_hash(deployment_hash)

    if deployment_parameters[:post_deploy_command]
      exit_statuses = []
      deployment_parameters[:post_deploy_command].split('||').each do |command|
        update_output_log("Post Deploy Command: #{command}")

        status = Open4::popen4(command) do |pid, stdin, stdout, stderr|
          update_output_log("Running post deploy command under PID: #{pid}")

          until stdout.eof? do
            update_output_log(stdout.gets)
            update_output_log(stderr.gets)
          end
        end 

        exit_statuses << status.exitstatus.to_i == 0 ? true : false 
      end
      
      { success: !exit_statuses.include?(false) }
    else
      true      
    end
  end

  private
  def merge_hash(deployment_hash)
    deployment_hash[:application][:deployment_parameters].merge(deployment_hash[:deployment_environment][:environment][:deployment_parameters]).merge(deployment_hash[:deployment_environment][:deployment][:deployment_parameters])
  end

  def build_cap_command(deployment_parameters)
    "cd #{deployment_parameters[:cap_deploy_directory]} ; cap #{deployment_parameters[:stage]} deploy #{deployment_parameters[:tag]}"
  end
end
